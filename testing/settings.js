const {__} = wp.i18n

export const settings = {
    name:         'super/testing',
    title:        __('Testing'),
    description:  __('Testing all base elements'),
    modalOptions: {},
    sideOptions:  {},
}
