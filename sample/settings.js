import {defaultColors} from '../../src/js/blocks/globals'

const {__} = wp.i18n

export const settings = {
    name:         'super/sample',
    title:        __('Sample'),
    description:  __('This is the best block'),
    icon:         {
        background: '#3d5a80',
        src:        'list-view',
        foreground: '#fff',
        size:       20,
    },
    category:     'super',
    keywords:     [],
    preview:      'https://unsplash.it/260/100',
    modalOptions: {
        title:            {
            type:        'text',
            label:       __('Title'),
            help:        __('This is the hint'),
            placeholder: __('placeholder'),
        },
        content:          {
            type:        'textarea',
            label:       __('Title'),
            help:        __('This is the hint'),
            placeholder: __('placeholder'),
        },
        fieldCheckbox:    {
            type:    'checkbox',
            label:   __('This is the label'),
            heading: __('This is the heading'),
            help:    __('Help'),
        },
        fieldRadio:       {
            type:    'radio',
            label:   __('Label'),
            help:    __('This is the help'),
            options: [
                {label: __('Option 1'), value: 'option1'},
                {label: __('Option 2'), value: 'option2'},
            ],
        },
        fieldRange:       {
            type:  'range',
            label: __('Range'),
            min:   1,
            max:   100,
            step:  10,
        },
        fieldSelect:      {
            type:    'select',
            label:   __('Label'),
            help:    __('Help'),
            options: [
                {label: __('Option 1'), value: 'option1'},
                {label: __('Option 2'), value: 'option2'},
            ],
        },
        fieldDate:        {
            type: 'date',
        },
        fieldDropMenu:    {
            type:     'dropMenu',
            label:    __('Label'),
            icon:     'move',
            controls: [
                {
                    title:   'Up',
                    icon:    'arrow-up-alt',
                    onClick: () => console.log('up'),
                },
            ],
        },
        fieldDropdown:    {
            type: 'dropdown',
        },
        inputSuggestions: {
            type:        'inputSuggestions',
            placeholder: __('Type a word'),
            suggestions: ['keyA', 'keyB', 'keyC', 'keyD'],
        },
        fieldImage:       {
            type:     'image',
            labels:   {
                title: __('Title'),
                name:  __('Name'),
            },
            icon:     'format-image',
            multiple: true,
        },
        fieldEditor:      {
            type:   'editor',
            // Conditional fields
            showIf: [
                {fieldSelect: 'option_1'},
                {fieldSelect: 'option_2'},
            ],
        },
    },
    sideOptions:  {
        title2:              {
            type:        'text',
            label:       __('Title side'),
            help:        __('This is the hint'),
            placeholder: __('placeholder'),
        },
        content2:            {
            type:        'textarea',
            label:       __('Title side'),
            help:        __('This is the hint'),
            placeholder: __('placeholder'),
        },
        fieldToggle:         {
            type:  'toggle',
            label: __('Label'),
            help:  __('help'),
        },
        fieldColor:          {
            type:                'color',
            disableCustomColors: true,
            colors:              Object.keys(defaultColors).map((name) => ({name, color: defaultColors[name]})),
        },
        fieldFontSizePicker: {
            type:             'fontSizePicker',
            fontSizes:        [
                {shortName: 'S', size: 12},
                {shortName: 'M', size: 16},
            ],
            fallbackFontSize: 16,
        },
    },
    supports:     {
        html: false,
    },
}
