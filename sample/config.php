<?php

namespace App\BaseBlocks\Sample;

use Super\Builder\AbstractConfigLoader;

class Config extends AbstractConfigLoader
{
    public function enqueue()
    {
        wp_enqueue_script('example-script-handle');

        if ($this->get('my_field') === 'test') {
            wp_enqueue_script('another-script');
        }
    }

    public function customCss()
    {
        $this->css('.some_class_maybe_related', 'height: 1500px');

        $this->css('[unique_block_class]', 'top: 200px');

        $this->css('[unique_block_class]', [
            'height' => 'auto',
            'max-width' => '800px',
            'margin' => '0 auto',
            'background' => '#ccc'
        ]);

        $this->css('[unique_block_class]', 'color: red');
        $this->css('[unique_block_class]', 'font-size: 40px; font-weight: 900');

        if ($this->get('fieldRange') === 71) {
            $this->css('[unique_block_class]', 'top: 100px');
        }

        if ($this->get('fieldSelect') === 'option2') {
            $this->css('.test', 'background: red', 'max-width: 540px');
        }
    }

    public function data()
    {
        // Example of adding or modifying data in the block attributes.
        if($this->get('fieldRange') == 51){
            $this->set('blog_posts', get_posts());
        }
    }
}

