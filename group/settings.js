const {InnerBlocks} = wp.editor
const {__}          = wp.i18n

export const settings = {
    name:           'super/group',
    title:          __('Group'),
    modalOptions:   {},
    sideOptions:    {},
    renderCallback: (obj) => {
        return (
            <div>
                <InnerBlocks/>
            </div>
        )
    },
    saveCallback:   () => {
        return (
            <div>
                <InnerBlocks.Content/>
            </div>
        )
    }
}
