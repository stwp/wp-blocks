import React from 'react'
import {Markup} from 'interweave'

const {__} = wp.i18n

export const settings = {
    name:           'super/headline',
    title:          __('Headline'),
    modalOptions:   {
        title:       {
            type:  'text',
            label: __('Title'),
        },
        description: {
            type:  'editor',
            label: __('Description'),
        },
        tag:         {
            type:    'select',
            label:   __('Tag'),
            options: [
                {label: __('H1'), value: 'h1'},
                {label: __('H2'), value: 'h2'},
                {label: __('H3'), value: 'h3'},
            ],
            grid:    3
        },
        align:       {
            type:    'select',
            label:   __('Align'),
            options: [
                {label: __('Left'), value: 'left'},
                {label: __('Center'), value: 'center'},
                {label: __('Right'), value: 'right'},
            ],
            grid:    3
        },
    },
    sideOptions:    {},
    renderCallback: (obj) => {
        const attr = obj.props.attributes
        const Tag  = attr.tag ? attr.tag : 'h2'

        let style = {
            textAlign: attr.align ? attr.align : 'initial'
        }

        return (
            <div style={style}>
                <Tag>{attr.title}</Tag>
                <div className="super-headline-description"><Markup content={attr.description}/></div>
            </div>
        )
    },
}
