<?php

namespace App\BaseBlocks\Headline;

use Super\Builder\AbstractConfigLoader;

class Config extends AbstractConfigLoader
{
    public function enqueue()
    {
        // Scripts and styles
    }

    public function customCss()
    {
        // Custom CSS
    }
}

